#!/bin/sh
#
#######################################################
# yangjianjian 2014.5.26
#
# 描述：本脚本是对服务器进行一些安全性的设置
#######################################################

## 更新centos最新的缺省安装包的版本
yum update

yum install make -y

## 只允许root通过ssh登录(先删除再添加避免脚本多次执行配置出错的问题)
sed -i '/^AllowUsers/d' /etc/ssh/sshd_config
sed -i 's/AllowUsers root/d' /etc/ssh/sshd_config
sed -i '1 a\AllowUsers root' /etc/ssh/sshd_config

## 更改SSH的端口号为22151（先注释掉所有已经设置的端口，再添加新的唯一端口号22151，避免阿里云上最新SSH脚本的变化）
sed -i 's/^Port /\#Port /g' /etc/ssh/sshd_config
sed -i 's/Port 22151/d' /etc/ssh/sshd_config
sed -i '2 a\Port 22151' /etc/ssh/sshd_config

## 公钥设置
# 首先，需要对SecureCRT中生成公密钥，把公钥通过
# SecureFX传到服务器上。生成的公钥名 Identity.pub
# 我们约定，把Identity.pub先放到/root目录下面
if [ ! -e "/root/.ssh" ]; then
	mkdir /root/.ssh
fi

if [ ! -e '*.pub' ]; then
	echo -e "*.pub NOT FOUND."
	exit
fi

## 复制到.ssh目录下，并改名为authorized_keys
cp -f *.pub /root/.ssh/authorized_keys

## ssh开启公钥
sed -i 's/^\#RSAAuthentication/RSAAuthentication/g' /etc/ssh/sshd_config
sed -i 's/^\#PubkeyAuthentication/PubkeyAuthentication/g' /etc/ssh/sshd_config
sed -i 's/^\#AuthorizedKeysFile/AuthorizedKeysFile/g' /etc/ssh/sshd_config

## 禁用密码方式登录服务器
while [ 1 ]; do
        stty erase '^H'  #去掉读入退格符
        read -p "Are you sure to disable the PasswordAuthentication ? y/n: " dis_pswd
        if [ $dis_pswd == 'y' -o $dis_pswd == 'Y' ]; then
                sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
                break
        elif [ $dis_pswd == 'n' -o $dis_pswd == 'N' ]; then
                break
        else
                read -p "Are you sure to disable the PasswordAuthentication ? y/n: " dis_pswd
                continue
        fi
done

##开启防火墙，如果有SVN服务器需要再添加一条端口为3690的记录
echo \
"*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
-A INPUT -p icmp -j DROP
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 22151 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 3690 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
-A FORWARD -j REJECT --reject-with icmp-host-prohibited
COMMIT"  >  /etc/sysconfig/iptables

service iptables restart
chkconfig iptables on

## 配置空闲超时退出时间
sed -i 's/^ClientAliveInterval/ClientAliveInterval 43200/g' /etc/ssh/sshd_config
sed -i 's/^ClientAliveCountMax/ClientAliveCountMax 0/g' /etc/ssh/sshd_config

## 禁止不安全的远程访问协议
sed -i 's/^IgnoreRhosts/IgnoreRhosts yes/g' /etc/ssh/sshd_config

## 重启sshd
service sshd restart

## 服务器禁止ping
cp /etc/rc.d/rc.local /etc/rc.d/rc.localbak
echo "
echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all" >> /etc/rc.d/rc.local
source /etc/rc.d/rc.local　

## 禁用无用的帐号
cp /etc/passwd /etc/passwd.bak
sed -i 's/^adm/\#adm/g' /etc/passwd
sed -i 's/^lp/\#lp/g' /etc/passwd
sed -i 's/^sync/\#sync/g' /etc/passwd
sed -i 's/^shutdown/\#shutdown/g' /etc/passwd
sed -i 's/^halt/\#halt/g' /etc/passwd
sed -i 's/^uucp/\#uucp/g' /etc/passwd
sed -i 's/^operator/\#operator/g' /etc/passwd
sed -i 's/^games/\#games/g' /etc/passwd
sed -i 's/^gopher/\#gohper/g' /etc/passwd
sed -i 's/^ftp/\#ftp/g' /etc/passwd

##禁用无用的组
cp /etc/group /etc/group.bak
sed -i 's/^adm/\#adm/g' /etc/group
sed -i 's/^lp/\#lp/g' /etc/group
sed -i 's/^uucp/\#uucp/g' /etc/group
sed -i 's/^games/\#games/g' /etc/group
sed -i 's/^dip/\#dip/g' /etc/group
sed -i 's/^news/\#news/g' /etc/group

## 限制用户Ctrl + Alt + Del
sed -i 's/^\#start on/start on/g' /etc/init/control-alt-delete.conf
sed -i 's/^\#exec \/sbin\/shutdown -r/exec \/sbin\/shutdown -r/g' /etc/init/control-alt-delete.conf

## 限制不同文件的权限
chmod -R 700 /etc/rc.d/init.d/*
chattr +i .bash_history
chmod 700 -R /usr/sbin
chmod 700 -R /usr/bin
##以下两个设置了的话，如何添加普通用户，这个用户登录时，会一闪而过，没有
##执行shell的权限
chmod 700 -R /sbin
chmod 700 -R /bin

yum install gcc -y

## 更新系统OpenSSL至最新的版本
openssl_v=openssl-1.0.1h
if [ ! -e "$openssl_v.tar.gz" ]; then
	wget http://www.openssl.org/source/$openssl_v.tar.gz
fi
tar zxf $openssl_v.tar.gz
cd $openssl_v
./config shared -fPIC --prefix=/usr/
make && make install
cd ..

echo -e "+-----------------------------------------+"
echo -e "|          Configuration over             |"
echo -e "+-----------------------------------------+"
