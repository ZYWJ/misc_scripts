#!/bin/sh
#
#########################################################
# yangjianjian 2014.6.5
#
# 1. 请先确保各参数已经设置正确了
# 2. 如果需要定时备份，请使用crontab -e 编辑“计划”
#########################################################

## 参数设置
dump_dir=/var/www/html/rmt/uploadfile/DataBaseDumps
user=
passwd=
dbname=

Time=`date '+%Y-%m-%d-%H-%M'`
dump_file_name=$dbname-$Time


## 开始备份
mysqldump -u $user -p$passwd $dbname > $dump_dir/$dump_file_name.sql

echo "已备份到：$dump_dir/$dump_file_name.sql"
