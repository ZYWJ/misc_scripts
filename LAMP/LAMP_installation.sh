#!/bin/sh
# 还有一些BUG，暂时无法真正一键安装哦。。
#######################################################################
# Date:   2014.5.25
# Author: yangjianjian
#
# Readme:
# 1. 建议先把软件源码包拷到目录下，减少下载时间
# 2. 请先对路径及一些参数做自定义的修改
# 3. 如果从Windows中通过SecureFX复制到Linux上，可能这个文本在Windows中
#    被编辑过，运行时会提示缺少结束符之类的错误。
#    解决方法：
#    用vim打开，在命令模式下输入:set fileformat=unix 保存即可
# 4. 关于首次登录MariaDB
#    第一次登录MariaDB,先使用mysql进入数据库，
#    然后选择mysql，修改root用户密码，
#    修改完密码后，记得运行flush privileges
#######################################################################



#=========================================================================#
#                              配置开始                                   #
#=========================================================================#

########################    【全局设置】     #############################
## MariaDB httpd PHP各版本号设置
mariadb_v=mariadb-5.5.38
httpd_v=httpd-2.4.9
php_v=php-5.4.28
## 要嘛本服务器IP要嘛...
servername=127.0.0.1
## LAMP程序包存放目录
lampdir=$(pwd)

#########################    【MariaDB设置】    ##########################
## MariaDB安装主目录
mariadb_prefix=/usr/local
# MariaDB 数据文件存放目录
mariadb_datadir=$mariadb_prefix/mariadb/data
## 临时存放目录
mariadb_sock_addr=$mariadb_prefix/mariadb/tmp


#########################    【Apache设置】    ###########################
## Apache HTTPD安装主目录
httpd=/usr/local/apache2/
## APR安装目录
apr=/usr/local/apr/
## Apr-util安装主目录 
apr_util=/usr/local/apr-util
## pcre安装主目录
pcre=/usr/local/pcre


##########################    【PHP设置】    #############################
## PHP安装主目录
php=/usr/local/php
## sed时,PHP的安装主目录
php_sed="\/usr\/local\/php"
## 设置环境变量时用
php_bin="\/usr\/local\/php\/bin"
## PHP配置文件路径
php_ini=$php/lib/php.ini
## APXS路径
apxs=$httpd/bin/apxs
## Apache文档目录
document_root=$httpd/htdocs/

#=========================================================================#
#                               配置结束                                  #
#=========================================================================#



## 每一轮安装后停留5秒，能让管理员看清提示信息
function wait_a_while() {
	for((i = 3; i > 0; i--))	
	do
		echo -e "Wait $i s"
		sleep 1
	done
}

## 首先确保是root权限
user=$(whoami)
if [ $user != 'root' ]; then
	echo -e "必须要root用户才可以执行下面的操作"
	exit
fi

## 确保管理员运行之前已经对这个脚本已经配置过了
while [ 1 ]; do 
	stty erase '^H'  #去掉读入退格符
	read -p "你确定已经在运行前，已配置过这里一些必需的参数了吗？[y/n]" r
	if [ $r == "n" -o $ == "N" ]; then
		exit  #退出脚本
	elif [ $r != "y" -a $r != "Y" ]; then
		continue  #不是y也不是n，那么继续询问
	else 
		break  #开始执行下面的脚本
	fi
done

######################################################
## 安装MariaDB
######################################################
echo -e "+---------------------------------+"
echo -e "|         开始安装MariaDB         |"
echo -e "+---------------------------------+"

## 确定是否存在$lampdir目录
if [ ! -d "$lampdir" ]; then
	mkdir $lampdir
fi

cd $lampdir

## 安装make
if [ ! `which make` ]; then
	echo -e "==== 安装 cmake ===="
	yum -y install make
fi

## 安装wget
if [ ! `which wget` ]; then
	echo -e "==== 安装 wget ===="
	yum -y install wget
fi

## 安装g++
if [ ! `which g++` ]; then
	echo -e "==== 安装 g++ ===="
	yum -y install gcc-c++
fi

## 安装cmake
if [ ! `which cmake` ]; then
	echo -e "==== 安装 cmake ===="
	yum -y install cmake
fi
wait_a_while

## 安装ncurses库 
echo -e "==== 安装 Install ncurses-devel library ====" 
yum -y insatll ncurses
yum -y install ncurses-devel

## 下载MariaDB 
if [ ! -e "$lampdir/$mariadb_v.tar.gz" ]; then
	echo -e "==== 下载MariaDB ===="
	cd $lampdir
	wget http://mirrors.scie.in/mariadb/$mariadb_v/source/$mariadb_v.tar.gz
fi

cd $lampdir
if [ ! -d "$mariadb_v" ]; then
	echo -e "解压$mariadb_v.tar.gz中..."
	tar zxf $mariadb_v.tar.gz	
fi
cd $mariadb_v/

echo -e "==== 3秒后开始创建Makefile ===="
wait_a_while
## 创建Makefile
#mkdir $mariadb_prefix/mariadb
#mkdir $mariadb_datadir
#mkdir $mariadb_prefix/tmp

cmake \
-DCMAKE_INSTALL_PREFIX=$mariadb_prefix/mariadb \
# 数据存储目录
-DMYSQL_DATADIR=$mariadb_datadir \
# UNIX socket文件
-DMYSQL_UNIX_ADDR=$mariadb_sock_addr/mysqld.sock \
# 默认字符集
-DDEFAULT_CHARSET=utf8 \
# 默认字符校对
-DDEFAULT_COLLATION=utf8_general_ci \
# TCP/IP端口
-DMYSQL_TCP_PORT=3306 \
# * ARCHIVE 引擎支持
-DWITH_ARCHIVE_STORAGE_ENGINE=1 \
# * ARIA 引擎支持
-DWITH_ARIA_STORAGE_ENGINE=1 \
# * BLACKHOLE 引擎支持
-DWITH_BLACKHOLE_STORAGE_ENGINE=1 \
# * FEDERATEDX 引擎支持
-DWITH_FEDERATEDX_STORAGE_ENGINE=1 \
# * PARTITION 引擎支持
-DWITH_PARTITION_STORAGE_ENGINE=1 \
# * PERFSCHEMA 引擎支持
-DWITH_PERFSCHEMA_STORAGE_ENGINE=1 \
# * SPHINX 引擎支持
-DWITH_SPHINX_STORAGE_ENGINE=1 \
# * XTRADB 支持
-DWITH_XTRADB_STORAGE_ENGINE=1 \
# * innoDB 引擎支持
-DWITH_INNOBASE_STORAGE_ENGINE=1 \
# * Myisam 引擎支持
-DWITH_MYISAM_STORAGE_ENGINE=1 \
# readline库
-DWITH_READLINE=1 \
# 启用加载本地数据
-DENABLED_LOCAL_INFILE=1 \
# 扩展支持编码 ( all | utf8,gbk,gb2312 | none )
-DWITH_EXTRA_CHARSETS=all \
# 扩展字符支持
-DEXTRA_CHARSETS=all

## 编译与安装 
make && make install
echo -e "+-----------------------------------------+"
echo -e "|           MariaDB 已经安装              |"
echo -e "+-----------------------------------------+"
wait_a_while

## 创建目录 
if [ ! -d "$mariadb_prefix/mariadb/etc/" ]; then
	mkdir -p $mariadb_prefix/mariadb/etc/
fi

if [ ! -d "$mariadb_prefix/mariadb/data/" ]; then
	mkdir -p $mariadb_prefix/mariadb/data/
fi

if [ ! -d "$mariadb_prefix/mariadb/tmp/" ]; then
	mkdir -p $mariadb_prefix/mariadb/tmp/
fi

## 添加服务
if [ -e "/etc/init.d/mariadb" ]; then
	sudo rm /etc/init.d/mariadb
fi
wait_a_while

cp support-files/mysql.server /etc/init.d/mariadb
chmod +x /etc/init.d/mariadb
chkconfig --add mariadb
chkconfig mariadb on

## 添加用户与用户组
groupadd mariadb
useradd -s /sbin/nologin -g mariadb mariadb

## 复制配置文件
cp -f support-files/my-medium.cnf /etc/my.cnf

## 赋权
chown mariadb:mariadb -R $mariadb_prefix/mariadb

wait_a_while
## 修改my.cnf
echo -e "==== 修改MariaDB的配置文件 ===="
sed "/mysqld]/a\pid-file = $mariadb_prefix/mariadb/data/mariadb.pid" -i /etc/my.cnf
sed "/mysqld]/a\datadir = $mariadb_prefix/mariadb/data" -i /etc/my.cnf
sed "/mysqld]/a\basedir = $mariadb_prefix/mariadb/" -i /etc/my.cnf
sed "/mysqld]/a\user = mariadb" -i /etc/my.cnf
sed "/\#innodb_lock_wait_timeout=50/a\log_error = $mariadb_prefix/mariadb/data/mariadb.err" -i /etc/my.cnf
sed "/\#^tmpdir/a\tmpdir=$mariadb_prefix/tmp" -i /etc/my.cnf

## 初始化数据库
$mariadb_prefix/mariadb/scripts/mysql_install_db --user=mariadb \
--basedir=$mariadb_prefix/mariadb \
--datadir=$mariadb_prefix/mariadb/data

## 创建软链接
ln -fs $mariadb_prefix/mariadb/bin/mysql /usr/bin/mysql
ln -fs $mariadb_prefix/mariadb/bin/mysqladmin /usr/bin/mysqladmin
ln -fs $mariadb_prefix/mariadb/bin/mysqldump /usr/bin/mysqldump
cd $lampdir

## 设置编码
echo -e "==== 修改MariaDB的配置文件 ===="
sed -i '/\[client\]/a default-character-set=utf8' /etc/my.cnf
sed -i '/\[mysql\]/a default-character-set=utf8' /etc/my.cnf
sed -i '/\[mysqld\]/a character-set-server=utf8' /etc/my.cnf

service mariadb start

###################################################
## 安装Apache HTTPD
###################################################
wait_a_while
echo -e "+-------------------------------------+"
echo -e "|        开始安装 Apache HTTPD        |"
echo -e "+-------------------------------------+"

cd $lampdir

## 安装依赖
if [ ! -e "/usr/local/apr" ]; then
	yum -y install apr
	if [ ! -e "$/home/apr-1.5.1.tar.gz" ]; then
		echo -e "==== 下载 apr ===="
		wget http://mirror.esocc.com/apache//apr/apr-1.5.1.tar.gz	
	fi

	if [ ! -e "apr-1.5.1" ]; then
		tar zxf apr-1.5.1.tar.gz
	fi

	cd apr-1.5.1
	./configure --prefix=$apr
	wait_a_while
	make && make install
	echo -e "==== apr 已经安装成功 ===="
	wait_a_while
	cd ..
fi

if [ ! -e "/usr/local/apr-util" ]; then
	yum -y install apr-util
	if [ ! -e "apr-util-1.5.3.tar.gz" ]; then
		echo -e "==== 下载 apr-util ===="
		wget http://mirror.esocc.com/apache/apr/apr-util-1.5.3.tar.gz
	fi

	if [ ! -e "apr-util-1.5.3" ]; then
		tar zxf apr-util-1.5.3.tar.gz
	fi

	cd apr-util-1.5.3
	./configure --with-apr=$apr
	wait_a_while
	make && make install
	echo -e "==== apr-util 已经安装成功 ===="
	wait_a_while
	cd .. 
fi

if [ ! -e "/usr/local/pcre" ]; then
	if [ ! -e "pcre-8.35.tar.gz" ]; then
		echo -e "==== 下载 pcre ===="
		wget http://jaist.dl.sourceforge.net/project/pcre/pcre/8.35/pcre-8.35.tar.gz
	fi
	
	if [ ! -e "pcre-8.35" ]; then
		tar zxf pcre-8.35.tar.gz
	fi

	cd pcre-8.35
	./configure --prefix=$pcre
	wait_a_while
	make && make install
	echo -e "==== pcre 已经安装成功 ===="
	wait_a_while
	cd ..
fi

## 下载HTTPD
if [ ! -e "$lampdir/$httpd_v.tar.bz2" ]; then
	if [ ! -e "$httpd_v" ]; then
		echo "==== 下载 Apache HTTPD ====\n"
		wget http://apache.dataguru.cn//httpd/$httpd_v.tar.bz2
	fi
fi

if [ ! -e  "$lampdir/$httpd_v" ]; then
	echo -e "解压 HTTPD..."
	tar jxf $httpd_v.tar.bz2
fi
cd $httpd_v
cp -r $lampdir/apr-1.5.1/ ./srclib/apr
cp -r $lampdir/apr-util-1.5.3/ ./srclib/apr-util

## confiugre
./configure  \
--prefix=$httpd \
--with-apr=$apr \
--with-apr-util=$apr_util \
--with-pcre=$pcre \
--with-included-apr \
--with-mpm=event \
--enable-so \
--enable-zlib \
--enable-rewrite \
--enable-modules=most \
--enable-mpms-shared=all
echo -e "httpd 配置结束"

## 编译与安装
make && make install
echo -e "+----------------------------------------------+"
echo -e "|           Apache HTTPD 已经安装成功          |"
echo -e "+----------------------------------------------+"
wait_a_while

## 安装lynx
yum -y install lynx

## 添加到服务
cp -f $httpd/bin/apachectl /etc/init.d/httpd

sed -i '2 a\### \
# Comments to support chkconfig on RedHat Linux \
# chkconfig: 2345 90 90 \
# description:http server \
###' /etc/init.d/httpd

chkconfig --add httpd
chkconfig httpd on

## 修改ServerName
sed -i "s/\#ServerName=www.example.com:80/ServerName=$servername:80/g" $httpd/conf/httpd.conf

## 隐藏apache在HTTP响应头中的版本号
# 先删除这两行配置，防止append多条同样的
sed -i '/ServerTokens Prod/d' $httpd/conf/httpd.conf
sed -i '/ServerSignature Off/d' $httpd/conf/httpd.conf
sed -i '$ a\ServerTokens Prod  \
ServerSignature Off' $httpd/conf/httpd.conf

## rewrite取消注释
sed -i 's/^\#LoadModule rewrite_module/LoadModule rewrite_module/g' $httpd/conf/httpd.conf

## 防止列出目录
sed -i 's/Options Indexes FollowSymLinks/Options FollowSymLinks/g' $httpd/conf/httpd.conf

## allowoverride all 
sed -i 's/AllowOverride None/AllowOverride All/g' $httpd/conf/httpd.conf

## HTTPD用户与组
groupadd apache
useradd -s /sbin/nologin -g apache apache
sed -i 's/^User/#User/g' $httpd/conf/httpd.conf
sed -i '/\#User/a\User apache' $httpd/conf/httpd.conf

## 屏蔽.svn目录
if [ -z "`grep '<Directory ~ ".svn">' $httpd/conf/httpd.conf`" ]; then
echo '
<Directory ~ ".svn">
        Order allow,deny
        Deny from all
</Directory>' >> $httpd/conf/httpd.conf
fi

#####################################################
## 安装PHP
#####################################################
wait_a_while
echo -e "+-------------------------------------+"
echo -e "|            开始安装 PHP             |"
echo -e "+-------------------------------------+"

cd $lampdir

## 安装依赖
echo -e "==== 安装 libxml2 ===="
yum install libxml2	-y
yum install libxml2-devel -y

## 下载PHP
if [ ! -e "$lampdir/$php_v.tar.bz2" ]; then
	echo -e "==== 下载 PHP ===="
	wget http://us1.php.net/distributions/$php_v.tar.bz2
fi

if [ ! -e "$php_v" ]; then
	echo -e "解压 PHP..."
	tar jxf $php_v.tar.bz2
fi

cd $php_v

## configure 
./configure \
--prefix=$php \
--with-apxs2=$apxs \
--with-mysql \
--disable-fileinfo

## 编译与安装
make && make install
echo -e "+-------------------------------------+"
echo -e "|          PHP 已经安装成功           |"
echo -e "+-------------------------------------+"
wait_a_while

## 添加到环境变量
echo "export PATH=$php/bin:\$PATH" >> /etc/profile

## 复制配置文件
if [ ! -e "$php/lib/php.ini" ]; then
	cp php.ini-development  $php/lib/php.ini
	
fi

## 隐藏HTTP响应头中的x-powered-by
# 先删除这行配置，防止append多条同样的
sed -i '/expose_php = Off/d' $php/lib/php.ini
sed -i '$ a\expose_php = Off' $php/lib/php.ini

## php的时间
sed -i 's/\;date.timezone =/date.timezone = Asia\/Shanghai/g' $php_ini

## 修改session存放的路径
sed -i "/session.save_path = $php_sed\/tmp/d" $php_ini
sed -i "/\[session\]/a\session.save_path = $php_sed\/tmp" $php_ini
chmod 777 $php/tmp

## 把PHP加到环境变量中
sed -i "/export PATH="$php_bin":\$PATH/d" /etc/profile
sed -i "$ a\export PATH="$php_bin":\$PATH" /etc/profile
source /etc/profile

## 安装autoconf
yum install autoconf -y

## php extesion 路径修改
php_extension="$php_sed\/lib\/php\/extensions"
sed -i "/extension_dir = $php_extension/d" $php_ini
sed -i "$ a\extension_dir = $php_extension" $php_ini

## mbstring扩展安装
cd $lampdir/php*/ext/mbstring
phpize
./configure --with-php-config=/usr/local/php/bin/php-config
make && make install
cd $php/lib/php/extensions/no-debug-zts-20100525/ 
mv mbstring.so ../
sed -i "/extension_dir = $php_sed\/lib\/php\/extensions/a\extension = \"mbstring.so\"" $php_ini

## 安装mecab
cd $lampdir
if [ ! -e "mecab-0.996.tar.gz" ]; then
	wget https://mecab.googlecode.com/files/mecab-0.996.tar.gz
fi

if [ ! -d "mecab-0.996" ]; then
	echo -e "解压 mecab..."
	tar zxf mecab-0.996.tar.gz
fi

cd mecab-0.996
./configure --with-charset=utf8
make && make install

## 安装mecab-ipadic
cd $lampdir
if [ ! -e "mecab-ipadic-2.7.0-20070801.tar.gz" ]; then
	wget https://mecab.googlecode.com/files/mecab-ipadic-2.7.0-20070801.tar.gz
fi

if [ ! -d "mecab-ipadic-2.7.0-20070801" ]; then
	echo -e "解压 mecab-ipadic..."
	tar zxf mecab-ipadic-2.7.0-20070801.tar.gz
fi

cd mecab-ipadic-2.7.0-20070801
./configure --with-charset=utf8 --enable-mutex
make && make install

## 安装php-mecab
cd $lampdir
if [ ! -e "php-mecab-master.zip" ]; then
	wget https://github.com/rsky/php-mecab/archive/master.zip
	mv master.zip php-mecab-master.zip
fi

if [ ! -d "php-mecab-master" ]; then
	unzip php-mecab-master.zip
fi

cd  php-mecab-master/mecab
phpize
./configure
make && make install
cd $php/lib/php/extensions/no-debug-zts-20100525/ 
mv mecab.so ../

sed -i '/extension=\"mecab.so\"/d' $php_ini
sed -i '/^extension_dir/a\extension=\"mecab.so\"' $php_ini

######################################################
## 关联HTTPD和PHP
######################################################

## 把inxex.php设为默认首页
sed -i 's/DirectoryIndex index.html/DirectoryIndex index.php index.html/g' $httpd/conf/httpd.conf

## 给httpd.conf添加两条记录
# 先删除这两行配置，防止append多条同样的
sed -i '/AddType application\/x-httpd-php .php/d' $httpd/conf/httpd.conf
sed -i '/AddType application\/x-httpd-php=source .phps/d' $httpd/conf/httpd.conf
sed -i '/AddType application\/x-gzip .gz .tgz/a AddType application\/x-httpd-php .php' $httpd/conf/httpd.conf
sed -i '/AddType application\/x-gzip .gz .tgz/a AddType application\/x-httpd-php=source .phps' $httpd/conf/httpd.conf

## 启动apache httpd
service httpd start

## 添加index.php
echo "<?php
phpinfo();" > $document_root/index.php

echo -e "+------------------------------------------------------------------------+"
echo -e "|    Congratulations! The LAMP environment is installed successfully!    |"
echo -e "+------------------------------------------------------------------------+"
