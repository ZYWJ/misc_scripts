#!/usr/bin/python
#condig=utf-8

####################################################
# yangjianjian 2014.6.6
#
# ReadMe
# 1. Please use python 2.X.
# 2. My environment is python 2.6.6
# 3. Please use English. I think this is due to the 
#	 Python version problem.
####################################################

from email.mime.text import MIMEText
import urllib2
import smtplib
import time
import os

mail_host = "smtp.163.com"
mail_user = "rmtmonitor"
mail_pass = "HELLO1234"
mail_postfix = "163.com"
content = "server down !"
die_msg = ""

#Please do not change it!
time_format = "YYYY-mm-dd HH:MM:SS"
time_format_len = len(time_format)


def is_DB_Amavis_Down(content):
	#get datetime
	tag_len = content.find('-', 1)
	t = content[tag_len  + 1 : time_format_len + tag_len + 1]

	try:
		t = time.strptime(t, "%Y-%m-%d %H:%M:%S")
	except:
		return True		

	return False


def Monitor(url_info):
	global die_msg
	tag_url = url_info.split(':', 1)
	
	tag = tag_url[0].strip(' ')
	url = tag_url[1].strip(' ')
	url = url.strip('\n')

	page_content = ''
	try:
		response = urllib2.urlopen(url) 
		page_content = response.read()

	except urllib2.HTTPError as e:
		die_msg = "HTTPD"
		#print('Error code:',e.code) 
		return False

	except urllib2.URLError as e:
		die_msg = "HTTPD"
		#print('Reason:', e.reason)			
		return False

	#print(page_content)

	if is_DB_Amavis_Down(page_content):
		tag_len = url_info.find(':', 1)

		if url_info[:tag_len] == 'C2J':
			die_msg = 'MariaDB'
		elif url_info[:tag_len] == 'RDS':
			die_msg = 'MariaDB'
		elif url_info[:tag_len] == 'EDM':
			die_msg = 'Amavis'
		elif url_info[:tag_len] == 'C2Jtmp':
			die_msg = 'MariaDB'
		return False
		
	return True

def SendMail(url_info):
	global die_msg
	#get tag
	tag_len = url_info.find(':', 1)
	tag = url_info[:tag_len]

	addr_list_file = open('addr_list', 'r')	
	addr_list = []
	for v in addr_list_file:
		v = v.strip('\n')
		if v != '' and v != '\n':
			addr_list.append(v)

	addr_list_file.close()

	for i in range(len(addr_list)):	
		sender = mail_user + "<" + mail_user + "@" + mail_postfix + ">" 
		msg = MIMEText(tag + " " + content + " because: " + die_msg);
		msg['Subject'] = tag + " server has down!!!"
		msg['From'] = sender
		msg['To'] = addr_list[i]
		
		try:
			s = smtplib.SMTP()
			s.connect(mail_host)
			s.login(mail_user, mail_pass)
			s.sendmail(sender, addr_list[i], msg.as_string())
			s.close()
		except:			 
			return False

	return True


def isOut30min(url_info):
	tag_len = url_info.find(':', 1)
	tag = url_info[:tag_len]

	fn = './log/last_send_time_' + tag + '.log'
	if os.path.exists(fn):
		f =  open(fn, 'r')
	else:
		f = open(fn, 'w')
		f.write(str(int(time.time())))
		return True

	last_send_time = f.read()
	f.close()

	last_send_time = last_send_time.strip('\n')
	last_send_time = int(last_send_time)
	
	if int(time.time()) - last_send_time >= 1800:	
		return True
	else:
		return False


def write_send_time(url_info):
	tag_len = url_info.find(':', 1)
	tag = url_info[:tag_len]

	fn = './log/last_send_time_' + tag + '.log'

	f = open(fn, 'w')
	f.write(str(int(time.time())))
	f.close()


def main():
	urls_file = open('Urls_For_Monitor', 'r');

	# Add valid url to urls
	urls_info = []
	for line in urls_file:
		if line != '' and line != '\n':
			urls_info.append(line)
	urls_file.close()

	# Traverse the Urls_For_DownTest file
	for i in range(len(urls_info)):

		c = urls_info[i].strip(' ')
		if c[0] != '#':
			if not Monitor(urls_info[i]):
			#if Monitor(urls_info[i]):
				time.sleep(15)
				if not Monitor(urls_info[i]):
					if isOut30min(urls_info[i]):				

						if SendMail(urls_info[i]):
							tag_len = urls_info[i].find(':', 1)
							tag = urls_info[i][:tag_len]

							print tag + ' server down. Have sent mail.'
							write_send_time(urls_info[i])			
						else:
							print 'Failed to send mail'
					else:
						print 'Smaller than 30mins before last mail sent.'
				else:
					print 'Everything is OK.'
			else: 
				print 'Everything is OK'


# start function. 
# It is said that in the 'crond', the function "if __name__ == '__main__'" does not work.
# So only use the user defined function.
main()


