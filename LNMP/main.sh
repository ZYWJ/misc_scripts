#!/bin/sh

##################################################
## Author: yangjianjian
## Date: 2014-8-2
##################################################

work_dir=$(pwd)

sh src/mysql.sh $work_dir
sleep 5
sh src/nginx.sh $work_dir
sleep 5
sh src/php.sh $work_dir