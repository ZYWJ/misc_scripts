#####################################################
##  Author: yangjianjian
##  Date: 2014-8-2
#####################################################
echo -e "+-------------------------------------+"
echo -e "|         Start installing PHP        |"
echo -e "+-------------------------------------+"

work_dir=$1
php_v="php-5.4.31"
php_url="http://cn2.php.net/distributions/$php_v.tar.gz"
php_prefix="/usr/local/php"
mysql_prefix="/usr/local/mysql"

cd $work_dir
## 安装依赖
echo -e "==== Install libxml2 ===="
yum install libxml2	-y
yum install libxml2-devel -y

## 下载PHP
cd pkg/
if [ ! -e "$work_dir/pkg/$php_v.tar.gz" ]; then
	echo -e "==== Download PHP ===="
	wget $php_url
fi

cd ../unzip
if [ ! -e "$php_v" ]; then
	cd ../pkg
	tar zvxf $php_v.tar.gz -C ../unzip
fi
cd $work_dir/unzip/$php_v

## configure 
./configure --prefix=$php_prefix --enable-fpm --disable-fileinfo --with-mysql=$mysql_prefix

## 编译与安装
make && make install
echo -e "+-------------------------------------+"
echo -e "|    PHP is installed successfully    |"
echo -e "+-------------------------------------+"

## 复制配置文件
cp -f php.ini-production  $php_prefix/lib/php.ini

echo '<?php
phpinfo();' > /var/www/html/index.php


php_prefix='/usr/local/php'
php_user='php-fpm'
php_group='php-fpm'

## 配置php-fpm
cd $php_prefix/etc/
mv php-fpm.conf.default php-fpm.conf

## 添加php用户
groupadd $php_group
useradd -s /sbin/nologin -g $php_group $php_user

sed -i 's/;pid = run\/php-fpm.pid/pid = run\/php-fpm.pid/g' php-fpm.conf
sed -i 's/;error_log = log\/php-fpm.log/error_log = log\/php-fpm.log/g' php-fpm.conf
sed -i "s/user = nobody/user = $php_user/g" php-fpm.conf
sed -i "s/group = nobody/group = $php_group/g" php-fpm.conf

## 隐藏HTTP响应头中的x-powered-by
sed -i '$ a\expose_php = Off' $php_prefix/lib/php.ini

##添加到服务
echo '#!/bin/sh  
# DateTime: 2013-09-16
# Author: lianbaikai
# site:http://www.ttlsa.com/html/3039.html
# chkconfig:   - 84 16   
# Source function library.  
. /etc/rc.d/init.d/functions  
 
# Source networking configuration.  
. /etc/sysconfig/network  
 
# Check that networking is up.  
[ "$NETWORKING" = "no" ] && exit 0  
 
phpfpm="/usr/local/php-5.3.10/sbin/php-fpm"  
prog=$(basename ${phpfpm})  
 
lockfile=/var/lock/subsys/phpfpm
 
start() {  
    [ -x ${phpfpm} ] || exit 5  
    echo -n $"Starting $prog: "  
    daemon ${phpfpm}
    retval=$?  
    echo  
    [ $retval -eq 0 ] && touch $lockfile  
    return $retval  
}  
 
stop() {  
    echo -n $"Stopping $prog: "  
    killproc $prog -QUIT  
    retval=$?  
    echo  
    [ $retval -eq 0 ] && rm -f $lockfile  
    return $retval  
}  
 
restart() {  
    configtest || return $?  
    stop  
    start  
}  
 
reload() {  
    configtest || return $?  
    echo -n $"Reloading $prog: "  
    killproc ${phpfpm} -HUP  
    RETVAL=$?  
    echo  
}  
 
force_reload() {  
    restart  
}  
 
configtest() {  
  ${phpfpm} -t
}  
 
rh_status() {  
    status $prog  
}  
 
rh_status_q() {  
    rh_status >/dev/null 2>&1  
}  
 
case "$1" in  
    start)  
        rh_status_q && exit 0  
        $1  
        ;;  
    stop)  
        rh_status_q || exit 0  
        $1  
        ;;  
    restart|configtest)  
        $1  
        ;;  
    reload)  
        rh_status_q || exit 7  
        $1  
        ;;  
    status)  
        rh_status  
        ;;  
    *)  
        echo $"Usage: $0 {start|stop|status|restart|reload|configtest}"  
        exit 2  
esac
' > /etc/init.d/php-fpm

chmod 777 /etc/init.d/php-fpm
chkconfig --add php-fpm
chkconfig php-fpm on

cd $php_prefix/lib
## 修改PHP的时区
sed -i '/^;date\.timezone/a\date\.timezone = Asia\/Shanghai' php.ini

if [ ! -d '/var/www/session_save_path' ]; then
	mkdir /var/www/session_save_path
fi
##修改session保存的路径
sed -i '/^;session.save_path/a\session.save_path = "/var/www/session_save_path/"' php.ini

## 添加到环境变量
sed -i "$ a\export PATH=$php_prefix/bin:\$PATH" /etc/profile
source /etc/profile

service php-fpm start
