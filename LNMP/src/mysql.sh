#!/bin/sh

#################################################
## Author: yangjianjian
## Date: 2014-8-2
#################################################

work_dir=$1
mysql_v="mysql-5.5.38"
mysql_url="http://cdn.mysql.com/Downloads/MySQL-5.5/$mysql_v.tar.gz"
mysql_prefix="/usr/local"
mysql_datadir="$mysql_prefix/mysql/data"
mysql_sock_addr="$mysql_prefix/mysql/tmp/mysqld.sock"

## 首先确保是root权限
user=$(whoami)
if [ $user != 'root' ]; then
	echo -e "Abort. You must use root account to continue."
	exit
fi

echo -e "+---------------------------------+"
echo -e "|     Start installing MySQL      |"
echo -e "+---------------------------------+"

## 安装wget
if [ ! `which wget` ]; then
	echo -e "==== Install wget ===="
	yum -y install wget
fi

## 安装g++
if [ ! `which g++` ]; then
	echo -e "==== Install g++ ===="
	yum -y install gcc-c++
fi

## 安装cmake
if [ ! `which cmake` ]; then
	echo -e "==== Install cmake ===="
	yum -y install cmake
fi

## 安装ncurses库 
echo -e "==== Install ncurses-devel library ===="
yum -y install ncurses-devel ncurses

## MySQL 
cd pkg/
if [ ! -e "$mysql_v.tar.gz" ]; then
	echo -e "==== Download MySQL ===="
	wget $mysql_url
fi

cd ../unzip
if [ ! -d "$mysql_v" ]; then
	cd ../pkg	
	tar zvxf $mysql_v.tar.gz -C ../unzip	
fi
cd ../unzip/$mysql_v

echo -e "==== 3秒后开始创建Makefile ===="

## 创建Makefile
cmake \
-DCMAKE_INSTALL_PREFIX=/usr/local/mysql \
# 数据存储目录
-DMYSQL_DATADIR=/usr/local/mysql/data \
# UNIX socket文件
-DMYSQL_UNIX_ADDR=/usr/local/mysql/tmp/mysqld.sock \
# 默认字符集
-DDEFAULT_CHARSET=utf8 \
# 默认字符校对
-DDEFAULT_COLLATION=utf8_general_ci \
# TCP/IP端口
-DMYSQL_TCP_PORT=3306 \
# * ARCHIVE 引擎支持
-DWITH_ARCHIVE_STORAGE_ENGINE=1 \
# * ARIA 引擎支持
-DWITH_ARIA_STORAGE_ENGINE=1 \
# * BLACKHOLE 引擎支持
-DWITH_BLACKHOLE_STORAGE_ENGINE=1 \
# * FEDERATEDX 引擎支持
-DWITH_FEDERATEDX_STORAGE_ENGINE=1 \
# * PARTITION 引擎支持
-DWITH_PARTITION_STORAGE_ENGINE=1 \
# * PERFSCHEMA 引擎支持
-DWITH_PERFSCHEMA_STORAGE_ENGINE=1 \
# * SPHINX 引擎支持
-DWITH_SPHINX_STORAGE_ENGINE=1 \
# * XTRADB 支持
-DWITH_XTRADB_STORAGE_ENGINE=1 \
# * innoDB 引擎支持
-DWITH_INNOBASE_STORAGE_ENGINE=1 \
# * Myisam 引擎支持
-DWITH_MYISAM_STORAGE_ENGINE=1 \
# readline库
-DWITH_READLINE=1 \
# 启用加载本地数据
-DENABLED_LOCAL_INFILE=1 \
# 扩展支持编码 ( all | utf8,gbk,gb2312 | none )
-DWITH_EXTRA_CHARSETS=all \
# 扩展字符支持
-DEXTRA_CHARSETS=all

## 编译与安装 
make && make install
echo -e "+-----------------------------------------+"
echo -e "|    MySQL is installed successfully      |"
echo -e "+-----------------------------------------+"

## 创建目录 
if [ ! -d "$mysql_prefix/mysql/etc/" ]; then
	mkdir -p $mysql_prefix/mysql/etc/
fi

if [ ! -d "$mysql_prefix/mysql/data/" ]; then
	mkdir -p $mysql_prefix/mysql/data/
fi

if [ ! -d "$mysql_prefix/mysql/tmp/" ]; then
	mkdir -p $mysql_prefix/mysql/tmp/
fi

## 添加服务
if [ -e "/etc/init.d/mysql" ]; then
	sudo rm /etc/init.d/mysql
fi

cp support-files/mysql.server /etc/init.d/mysql
chmod +x /etc/init.d/mysql
chkconfig --add mysql
chkconfig mysql on

## 添加用户与用户组
groupadd mysql
useradd -s /sbin/nologin -g mysql mysql

## 复制配置文件
cp -f support-files/my-huge.cnf /etc/my.cnf

## 赋权
chown mysql:mysql -R $mysql_prefix/mysql

## 初始化数据库
$mysql_prefix/mysql/scripts/mysql_install_db --user=mysql --basedir=$mysql_prefix/mysql --datadir=$mysql_datadir

## 创建软链接
ln -fs $mysql_prefix/mysql/bin/mysql /usr/bin/mysql
ln -fs $mysql_prefix/mysql/bin/mysqladmin /usr/bin/mysqladmin
ln -fs $mysql_prefix/mysql/bin/mysqldump /usr/bin/mysqldump

## 设置编码
echo -e "==== 修改MariaDB的配置文件 ===="
sed -i '/\[client\]/a default-character-set=utf8' /etc/my.cnf
sed -i '/\[mysql\]/a default-character-set=utf8' /etc/my.cnf
sed -i '/\[mysqld\]/a character-set-server=utf8' /etc/my.cnf

service mysql start
