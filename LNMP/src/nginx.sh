#!/bin/sh
###################################################
## Author: yangjianjian
## Date: 2014-8-2
####################################################

work_dir=$1
cd $work_dir
nginx_v="tengine-1.5.2"
nginx_url="http://tengine.org/download/tengine-1.5.2.tar.gz"

## 添加用户与组
groupadd nginx
useradd -s /sbin/nologin -g nginx nginx

cd pkg/
## 下载pcre 
if [ ! -e 'pcre-8.35.tar.gz' ]; then
	wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.35.tar.gz
fi
tar zxf pcre-8.35.tar.gz -C ../unzip

## 下载 openssl
if [ ! -e 'openssl-1.0.1h.tar.gz' ]; then
	wget http://www.openssl.org/source/openssl-1.0.1h.tar.gz
fi
tar zxf openssl-1.0.1h.tar.gz -C ../unzip

tar zxf zlib-1.2.8.tar.gz -C ../unzip

## 下载nginx
if [ ! -e "$nginx_v.tar.gz" ]; then
	wget $nginx_url
fi

tar zxf $nginx_v.tar.gz -C ../unzip
cd ../unzip/$nginx_v

./configure --user=nginx \
--group=nginx  \
--with-pcre=../pcre-8.35  \
--with-openssl=../openssl-1.0.1h \
--with-zlib=../zlib-1.2.8

make && make install

## 添加到服务
cd $work_dir/src
cp -f $work_dir/src/nginx.service /etc/init.d/nginx

chmod 777 /etc/init.d/nginx
chkconfig --add nginx
chkconfig nginx on

## 开启服务
service nginx start

## 配置
cd $work_dir
nginx_dir='/usr/local/nginx'
if [ ! -d '/var/www' ]; then
        mkdir /var/www
fi

if [ ! -d '/var/www/html' ]; then
        mkdir /var/www/html
fi
cp -f $work_dir/src/nginx.conf  $nginx_dir/conf
service nginx restart
